package com.example.dateandtime

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var calendar: Calendar
   private lateinit var simpleDateFormat: SimpleDateFormat
    private var Date: String?=null
    var text: TextView?=null
    var btn: Button?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        text=findViewById(R.id.text)
        btn=findViewById(R.id.btn)
        calendar=Calendar.getInstance()
        simpleDateFormat=SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault())
        Date=simpleDateFormat.format(calendar.time)

        btn!!.setOnClickListener(View.OnClickListener { text!!.setText(Date) })
    }
}